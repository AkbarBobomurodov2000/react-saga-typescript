import {all} from 'redux-saga/effects'
import { helloSaga } from "Components/Content/content.saga";
import {sagaLogin} from 'Redux/auth/auth.saga';

export default function* rootSaga() {
  yield all([
    helloSaga(),
    sagaLogin()
  ])
  // code after all-effect
}

