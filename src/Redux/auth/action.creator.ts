import {IAuthLoginSuccess, IAuthLoginError} from './api.response.types';
import ACTION_TYPES from 'Redux/action.constant';
// ANCHOR types
import {ILoginFormValues} from 'Modules/Auth/Types/form'; 

export interface ILoading{
    type: typeof ACTION_TYPES.LOGIN_PENDING,
    payload:ILoginFormValues
}

export function handleLoading(data:ILoginFormValues):ILoading{
  
    return{
        type:ACTION_TYPES.LOGIN_PENDING,
        payload:data
    }
}

export interface ISuccess{
    type:typeof ACTION_TYPES.LOGIN_SUCCESS;
    payload:IAuthLoginSuccess;
}

export function handleSuccess(response:IAuthLoginSuccess):ISuccess{
   return{
       type:ACTION_TYPES.LOGIN_SUCCESS,
       payload:response
   }
}

export interface IFail{
    type:typeof ACTION_TYPES.LOGIN_FAIL;
    payload:IAuthLoginError
}

export function handleFail(error:IAuthLoginError):IFail{
    return{
        type:ACTION_TYPES.LOGIN_FAIL,
        payload:error
    }
}