import {fork, take, call, put, cancel, cancelled} from 'redux-saga/effects';
import ACTION_TYPES from 'Redux/action.constant';
import browserStorage from 'Services/storage';
// ANCHOR types
import {ILoginFormValues} from 'Modules/Auth/Types/form';

import {login} from './api';

import * as actionCreator from './action.creator';
import { IAuthLoginSuccess } from './api.response.types';



type setStorage<T, K> = (key:K , data:T | string)=>void | undefined;

function* authorize(data:Omit<ILoginFormValues,'showPassword'>){
    try{
      const response: IAuthLoginSuccess=yield call(login, data)

      console.log(response,'success')

      yield call<setStorage<string,string>>(browserStorage.set,'token', response.access_token);

      yield put(actionCreator.handleSuccess(response))
      
    }catch(e){
        yield put(actionCreator.handleFail(e));
    }finally{
       if(yield cancelled()){
          put({type:ACTION_TYPES.RESET_TOKEN})
       }
    }
}


export function* sagaLogin(){
    while(true){
      const data = yield take(ACTION_TYPES.LOGIN_PENDING);
      delete data.payload.showPassword
      
      const currentTask  = yield fork(authorize, data.payload);
      
      const action = yield take([ACTION_TYPES.LOGOUT, ACTION_TYPES.LOGIN_FAIL]);

      if(action.type === ACTION_TYPES.LOGOUT){
        yield cancel(currentTask);
        yield call(browserStorage.remove,'token')
      }

    }
}


