import {AnyAction} from 'redux';
import ACTION_TYPES from '../action.constant';
import {IAuthLoginSuccess, IAuthLoginError} from './api.response.types';


export interface ILoginState{
    status:string;
    loading:any;
    mainData:Partial<IAuthLoginSuccess>,
    fail:Partial<IAuthLoginError>
}

const initialState={
    status:'initial',
    loading:null,
    mainData:{},
    fail:{}
}

const loginReducer=(state:ILoginState=initialState,{type, payload}:AnyAction):ILoginState=>{
    switch(type){
       case ACTION_TYPES.LOGIN_PENDING:
           return{
               ...state,
               status:'loading'
           }
        case ACTION_TYPES.LOGIN_SUCCESS:
            return{
                ...state,
                mainData:payload,
                status:'success'
            }
        case ACTION_TYPES.LOGIN_FAIL:
            return{
                ...state,
                fail:payload,
                status:'fail'
            }
        case ACTION_TYPES.RESET_TOKEN:
            return initialState;
       default:
           return state;
    }
}

export default loginReducer;