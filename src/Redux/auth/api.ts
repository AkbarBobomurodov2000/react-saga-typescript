import {AxiosResponse, AxiosError, AxiosPromise} from 'axios';
import {ApiService, RequestBody,RequestParams, client} from 'Services/api/api';

// ANCHOR types
import {ILoginFormValues} from 'Modules/Auth/Types/form';
import {IAuthLoginSuccess, IAuthLoginError} from './api.response.types';
export type paramsType = Omit<ILoginFormValues,'showPassword'>;



export const login= async (data:Omit<ILoginFormValues,'showPassword'>):Promise<IAuthLoginSuccess | IAuthLoginError>=>{
    try{
        
        const params=new RequestParams<paramsType>(data).requestParams;

        const api=new ApiService('').setRequestUrl('auth/login').setMethod('POST').request<null,paramsType>(null,params);

        const  response: AxiosResponse<IAuthLoginSuccess> = await client(api)

        console.log(response);

        return response.data

    } catch(e){
    
        console.log(e.response.data)

        throw e.response.data;
    }
}

