
export interface IUser{
    id:number;
    name:string;
    email:string;
    phone_number:number;
    created_at:string;
    activated_at:string;
}

export interface IUserRole extends Omit<IUser, 'email' | 'phone_number'>{
   label:string
}

// NOTE success
export interface IAuthLoginSuccess{
    status:string;
    user:IUser;
    user_role:IUserRole;
    access_token:string;
}

// NOTE fail
export interface IAuthLoginError{
    status:string;
    message:string;
}