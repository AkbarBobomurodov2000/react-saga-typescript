enum ACTION_TYPES {
  INCREMENT = "counter/INCREMENT",
  DECREMENT = "counter/DECREMENT",
  LOGIN_PENDING='login/pending',
  LOGIN_SUCCESS='login/success',
  LOGIN_FAIL='login/fail',
  LOGOUT='login/logout',
  RESET_TOKEN='login/resetToken'
}

export default ACTION_TYPES;
