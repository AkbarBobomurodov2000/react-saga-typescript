import { createStore, applyMiddleware, Store, compose } from "redux";
import createSagaMiddleware from "redux-saga";

import rootReducer, { RootState } from "./root.reducer";
import rootSaga from "./index.saga";

const sagaMiddleware = createSagaMiddleware();

const store: Store<RootState> = createStore(
  rootReducer,
  compose( applyMiddleware(sagaMiddleware), (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__() )
);

sagaMiddleware.run(rootSaga);

export default store;
