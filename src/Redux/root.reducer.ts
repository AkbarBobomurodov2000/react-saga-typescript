import {
  combineReducers,
  CombinedState,
  ReducersMapObject,
  compose,
} from "redux";

import reducers from "./index.reducer";

const rootReducer = combineReducers(reducers);

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
