import counterReducer from "Components/Content/content.reducer";
import loginReducer from 'Redux/auth/auth.reducer';
export default {
  counters: counterReducer,
  login:loginReducer
};
