import { createSelector, Selector } from "reselect";
import { AnyAction } from "redux";
import { RootState } from "Redux/root.reducer";

import ACTION_TYPES from "Redux/action.constant";

export interface CounterState {
  count: number;
}

export interface IIncrementActionType {
  type: typeof ACTION_TYPES.INCREMENT;
  payload: CounterState;
}

export interface IDecrementActionType {
  type: typeof ACTION_TYPES.DECREMENT;
  payload: CounterState;
}

const initialState: CounterState = {
  count: 0,
};

export type counterActionTypes = IIncrementActionType | IDecrementActionType;

const counterReducer = (
  state: CounterState = initialState,
  action: AnyAction
): CounterState => {
  const { type, payload } = action;

  switch (type) {
    case ACTION_TYPES.INCREMENT:
      return {
        ...state,
        count: state.count + payload.count,
      };
    case ACTION_TYPES.DECREMENT:
      return {
        ...state,
        count: state.count - payload.count,
      };
    default:
      return state;
  }
};

export const selectCounters = (state: RootState): CounterState =>
  state.counters;

export const selectCount = createSelector<
  RootState,
  CounterState,
  CounterState["count"]
>(selectCounters, (counters) => counters.count);

export default counterReducer;
