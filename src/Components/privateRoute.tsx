import React, { memo } from 'react';
import { Route, Redirect, RouteProps } from 'react-router-dom';

import {IRoute} from 'routes.path'

export interface IPrivateProps extends  IRoute{
	children:JSX.Element | null
}

const PrivateRoute:React.FC<IPrivateProps> = ({ children, ...rest }) => {
  if (!localStorage.getItem('token')) return <Redirect to="/" />;
  return (
    <Route
      {...rest}
      render={({ location }) =>
        true ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: '/',
              state: { from: location }
            }}
          />
        )
      }
    />
  );
};



export default PrivateRoute ;
