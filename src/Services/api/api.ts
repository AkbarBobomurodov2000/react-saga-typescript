import axios,{ResponseType} from "axios";

import {KeyValue, ApiMethod} from '../../types';

export const client = axios.create({
  baseURL: "http://sudex.napaautomative.com/"
});



export class ApiService{

  private _method:ApiMethod='POST';

  private _headers:string[][]=[];

  private _baseUrl:string='http://sudex.napaautomative.com/';

  private _requestUrl:string=''

  constructor(private _authToken:string){
  }

  get authToken():string{
    return this._authToken
  }

  set authToken(newAuthToken:string){
    this._authToken=newAuthToken;
  }

  public setRequestUrl(newRequestUrl:string):ApiService{
    this._requestUrl=`${this._baseUrl}${newRequestUrl}`;
    return this;
  }

  public setMethod(newMethod:ApiMethod){
    this._method=newMethod;
    return this;
  }

  public setHeaders(headers:KeyValue<string,string>[]):ApiService{

    for (const i in headers) {
      if (headers[i].hasOwnProperty('key')
        && headers[i].hasOwnProperty('value')) {
        
         this._headers.push([
           headers[i].key, 
           headers[i].value
         ]);
      }
    }

    return this;
  }

  get headers():string[][]{
    return this._headers
  }

  public resetHeaders(){
    this._headers=[];
  }

  public request<T, K> (data: T, params: K): any {
    return {
       url:this._requestUrl,
       headers: this._headers,
       method: this._method,
       data,
       params
    }
 }

}

export class RequestBody<T> {
  constructor (private _requestBody: T) {
  }
  get requestBody (): T {
    return this._requestBody;
  }
  set requestBody (newRequestBody: T) {
     this._requestBody = newRequestBody;
  }
}


export class RequestParams<T>{
  constructor(private _requestParams:T){

  }

  get requestParams():T{
    return this._requestParams
  }

  set requestParams(newRequestParams:T){
    this._requestParams=newRequestParams;
  }
}

