import {
  useSelector as useSelectData,
  TypedUseSelectorHook,
} from "react-redux";

import { RootState } from "../Redux/root.reducer";

const useSelector: TypedUseSelectorHook<RootState> = useSelectData;

export default useSelector;
