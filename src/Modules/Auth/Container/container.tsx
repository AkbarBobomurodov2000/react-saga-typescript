import React,{useState} from 'react';
import { useDispatch } from 'react-redux';
// ANCHOR api 
import * as action from 'Redux/auth/action.creator';
// ANCHOR Components
import Form from 'Modules/Auth/Components/form';

// ANCHOR types
import {ILoginFormValues} from 'Modules/Auth/Types/form'; 


interface ILoginContainerState{
	checked:boolean;
	values:ILoginFormValues
}

const LoginContainer=()=>{
   const dispatch = useDispatch();
   const [checked, setChecked] = React.useState(false);

	const [values, setValues] = useState<ILoginFormValues>({
		email: "",
		password: "",
		showPassword: false
	});

	// form elements handler

	const handleChange = (prop: keyof ILoginFormValues) => (
		event: React.ChangeEvent<HTMLInputElement>
	) => {
		setValues({ ...values, [prop]: event.target.value });
	};

	// show password action handler

	const handleClickShowPassword = () => {
		setValues({ ...values, showPassword: !values.showPassword });
	};

	const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();

		console.log('values', values)

	   dispatch(action.handleLoading(values))
	};



	return(
         <section>
         	  <Form onHandleChange={handleChange} onHandleClickShowPassword={handleClickShowPassword} onHandleSubmit={handleSubmit} values={values} />
         </section>
		)
}

export default LoginContainer;

