import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import IconButton from "@material-ui/core/IconButton";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import InputLabel from "@material-ui/core/InputLabel";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import Button from "@material-ui/core/Button";

// ANCHOR types
import {ILoginFormValues} from 'Modules/Auth/Types/form'; 
// styles
import { useStyles } from "./form.style";

// ctrl alt f format

interface IFormProps{
	onHandleChange:(prop: keyof ILoginFormValues)=>(event:React.ChangeEvent<HTMLInputElement>)=>void;
	onHandleClickShowPassword:()=>void;
	onHandleSubmit:(event:React.FormEvent<HTMLFormElement>)=>void;
	values:ILoginFormValues
}

const Form:React.FC<IFormProps> = (props) => {
	const classes = useStyles();

	const {values,onHandleChange,onHandleClickShowPassword, onHandleSubmit} = props;
	
	return (
		<div className={classes.root}>
			<Grid container justify="center" alignItems="center">
				<Grid item lg={3}>
					<h1>Logo</h1>
					<form onSubmit={onHandleSubmit}>
						<div className={classes.form_container}>
							<TextField
								id="email"
								label="Email"
								variant="outlined"
								onChange={onHandleChange("email")}
							/>

							<FormControl variant="outlined">
								<InputLabel htmlFor="outlined-adornment-password">
									Password
								</InputLabel>
								<OutlinedInput
									id="outlined-adornment-password"
									type={
										values.showPassword
											? "text"
											: "password"
									}
									value={values.password}
									onChange={onHandleChange("password")}
									endAdornment={
										<InputAdornment position="end">
											<IconButton
												aria-label="toggle password visibility"
												onClick={
													onHandleClickShowPassword
												}
												// onMouseDown={
												// 	handleMouseDownPassword
												// }
												edge="end"
											>
												{values.showPassword ? (
													<Visibility />
												) : (
													<VisibilityOff />
												)}
											</IconButton>
										</InputAdornment>
									}
									labelWidth={70}
								/>
							</FormControl>

						
							<Button
								variant="contained"
								color="primary"
								type='submit'
							>
								Log in
							</Button>
						</div>
					</form>
				</Grid>
			</Grid>
		</div>
	);
};

export default Form;
