import Grid from '@material-ui/core/Grid';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      padding:0,
      margin:0,
      height:'100vh',
      display:'flex',
      alignItems:'center'
    },
    paper: {
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    form_container:{
      height:'30%',
      display:'flex',
      flexDirection:'column',
      gap:'10px'
    },
    form_checkbox_el:{
      justifySelf:'flex-start',
    }
  }),
);

