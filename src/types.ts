export type KeyValue<T,U>={
    key:T,
    value:U
}

export type ApiMethod='GET'| 'POST' |'DELETE'|'PUT';

