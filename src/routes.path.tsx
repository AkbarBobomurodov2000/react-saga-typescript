import React, {lazy, Suspense} from 'react';


// Project Pages
const LoginPage = lazy(()=>import('Pages/login'))
const Landing=lazy(()=>import('Landing'))

export interface IRoute{
	private:boolean;
	component?:React.ExoticComponent,
	path:string,
	exact?:boolean
}

export const routes: Array<IRoute>=[
{
	private:false,
	component:Landing,
	path:'/'
},
{
	private:false,
	component:LoginPage,
	path:'/login'
}
]