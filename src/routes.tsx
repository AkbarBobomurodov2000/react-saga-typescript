import React,{lazy, Suspense, FC, Fragment} from 'react';
import {BrowserRouter as Router, Route, Switch, RouteComponentProps, SwitchProps, RouteProps} from 'react-router-dom';

// custom components import
import PrivateRoute from 'Components/privateRoute';

// routes
import {routes} from './routes.path';



const Routes: FC<{}>=()=>{


	return <Router>
            <Suspense fallback={<>Loading...</>}>
               <Switch>
                 {
          		routes.map((route, index)=>{
          			if(route.private) return  <PrivateRoute exact {...route}><Route exact {...route}/></PrivateRoute>
						return  <Route  exact {...route}/>
          		})
          		}
               </Switch>
            </Suspense>
          </Router>
		
}

export default Routes;
